
package Conexao;

import java.util.List;


/**
 *
 * @author sala304b
 * @param <T>
 */
public interface DAO<T> {
    
    void inserir(T objeto);
    void atualizar(T objeto);
    void delete(int id);
    List<T> listarTodos();
    T buscarPorId(int id);
    
}
