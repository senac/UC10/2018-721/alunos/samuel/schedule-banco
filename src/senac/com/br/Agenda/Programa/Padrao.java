package senac.com.br.Agenda.Programa;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public abstract class Padrao extends JFrame {

    public Padrao() {

        this.setVisible(true);
    }

    public void showMessageInformacao(String mensagem) {
        JOptionPane.showMessageDialog(this, mensagem, "AVISO", JOptionPane.INFORMATION_MESSAGE);
    }

    public void showMessageErro(String mensagem) {
        JOptionPane.showMessageDialog(this, mensagem, "ERRO", JOptionPane.ERROR_MESSAGE);
    }

    public void showMessageAlerta(String mensagem) {
        JOptionPane.showMessageDialog(this, mensagem, "ALERTA", JOptionPane.WARNING_MESSAGE);
    }
    
    public void showMessageInformacaoB(String mensagem) {
        JOptionPane.showMessageDialog(this, mensagem, "Autenticado com sucesso", JOptionPane.INFORMATION_MESSAGE);
    }

}
